import React from 'react';

class Modal extends React.Component {
	constructor(props) {
		super(props);
	}
	submit(e) {
		e.preventDefault();
		var rowData = {};
		this.props.titles.forEach((title) => {rowData[title] = this.refs[title].value}); // формування новоствореного рядка-об'єкта
		this.props.submitListener(rowData, this.props.idx);
	}
	close(e) {
		if(e.target == this.refs.back) { // Перевірка, чи користувач натиснув на темну область вікна
			this.props.closeModal();
		}
	}
	render() {
		if(!this.props.visible) return null;
		var inputs = this.props.titles.map((title) => {
						return (<div>
							<h5 className="input-title">{title}</h5>
							<input type="text" placeholder={title} id="name" required pattern="\S(.*\S)?"
								ref={title} 
								defaultValue={this.props.data[title]}/>
						</div>)
					});
		return (
			<div className="modal-back" onClick={this.close.bind(this)} ref="back">
				<form className="popupForm" onSubmit={this.submit.bind(this)}>
					<h4 className="popupTitle centered">{this.props.title}</h4>
					
					{inputs}
					
					<input type="submit" className="btn-save" value={this.props.btn_text}/>
				</form>
			</div>
		)
	}
}

export default Modal;