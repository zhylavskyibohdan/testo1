import React from 'react';
import ReactDOM from 'react-dom';
import Control from './Control.js';
import MyComponent from './MyComponent.js';

class Page extends React.Component { /*Головний компонент*/
	constructor(props) {
		super(props);
		this.state = {
			appShowed: this._isAuthorized()
		}
	}
	_getUserData() {
		return JSON.parse(localStorage.getItem("user"));
	}	
	_saveUserData(newUserData) {
		localStorage.setItem("user", JSON.stringify(newUserData));
	}
	/* Метод для зберігання інформації про те, що користувач вже авторизований */
	_authorize() { 
		var userData = this._getUserData();
		userData.isAuthorized = true;
		this._saveUserData(userData);
	}
	/* Перевірка чи користувач вже авторизований*/
	_isAuthorized() {
		var userData = this._getUserData();
		if(userData && userData.isAuthorized) return true;

		return false;
	}
	/*Метод для видалення мітки про авторизацію користувача. Тобто, після виконання даного методу, користувач
	 буде переведений на головну сторінку, в якій буде відображена форма для авторизації
	*/
	_unAuthorize() {
		var userData = this._getUserData();
		userData.isAuthorized = false;
		this._saveUserData(userData);
		window.location.reload();
	}
	/*Службовий метод для відображення основної частини застосунку. Після його виконання, 
	форма авторизації буде прихована, натомість буде відображена основна частина.
	*/
	showApp() {
		this.setState({
			appShowed: true
		})
	}
	render() {
		if(!this.state.appShowed) {
			return (
				<div>
					<LoginForm showApp={this.showApp.bind(this)} 
						authorize={this._authorize.bind(this)}
						_getUserData={this._getUserData.bind(this)}
						_saveUserData={this._saveUserData.bind(this)}/>
				</div>
			); 
		}
		return (
			<div>
				<MyComponent unAuthorize={this._unAuthorize.bind(this)}/>
			</div>
		);
	}
}

/* Форма авторизації */
class LoginForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			warningVisible: false // Використовується для відображення попередження
		}
	}
	_checkCredentials(login, password) {
		var user = this.props._getUserData();
		if(!user) {
			this.props._saveUserData({login: "1", password: "1"});
			return this._checkCredentials(login, password);
		}
		return user.login == login && user.password == password;
	}
	
	_submit(e) {
		e.preventDefault();
		var refs = this.refs,
			login = refs.login.value,
			password = refs.password.value;

		if(this._checkCredentials(login,password)) {
			this.props.showApp();
			this.props.authorize();
		} else {
			this.setState({
				warningVisible: true
			})
		}
	}
	render() {
		return (
			<div className="login-form-back modal-back">
				<form className="settings-form" onSubmit={this._submit.bind(this)}>
					<span className="title">Sign In</span>
					<div className="login-input">
						<input type="text" placeholder="Login" ref="login" required pattern="\S(.*\S)?"/>
					</div>
					<div className="password-input">
						<input type="password" placeholder="Password" ref="password" required pattern="\S+"/>
					</div>
					<span className={this.state.warningVisible ? "login-warning":"hidden"}>Sorry, Login or Password is incorrect</span>
					<input type="submit" value="Login"/>
				</form>
			</div>
		);
	}
}
ReactDOM.render(<Page/>, document.querySelector('.wrapper'));