import React from 'react';

/*Головне "меню"*/
class Control extends React.Component {
	constructor(props) {
		super(props);
		this.state ={
			settingsVisible: false, // Активність форми налаштувань
			autoSave: false
		}
	}
	// Активація форми налаштувань
	settingsHandle() {
		this.setState({
			settingsVisible: !this.state.settingsVisible
		})
	}
	// Знищення форми налаштувань
	closeSettings() {
		this.setState({
			settingsVisible: !this.state.settingsVisible
		})
	}
	// Активація та деактивація автозбереження
	toggleSave(e) {
		this.props.setAutoSave(e.target.checked);
	}
	render() {
		return (
			<div className="control">
				<button href="#" className="control-item settings settings-btn" onClick={this.settingsHandle.bind(this)}>
					Settings
				</button>
				<div className="switch control-item">
				  <input id="cmn-toggle-1" className="cmn-toggle cmn-toggle-round" type="checkbox" checked={this.props.autoSave} onChange={this.toggleSave.bind(this)}/>
				  <label htmlFor="cmn-toggle-1"></label>
				</div>
				<button href="#" className={"control-item settings settings-save-btn " + (this.props.autoSave ? "hidden" : "")} onClick={this.props.saveRowsData}>
					Save
				</button>
				<button href="#" className={"control-item settings settings-logout-btn"} onClick={this.props.unAuthorize}>
					Logout
				</button>
				<SettingsModal 
					visible={this.state.settingsVisible} 
					closeSettings={this.closeSettings.bind(this)}/>
			</div>
		)
	}
}

/*Форма для налаштування профілю*/
class SettingsModal extends React.Component {
	constructor(props) {
		super(props);
	}
	_close(e) {
		if(e.target != this.refs.back) return;
		this.props.closeSettings();
	}
	_getUserData() {
		return JSON.parse(localStorage.getItem("user"));
	}
	_saveUserData(e) {
		e.preventDefault()
		var newUserData = {
			login: this.refs.login.value,
			password: this.refs.password.value
		};
		localStorage.setItem("user", JSON.stringify(Object.assign(this._getUserData(), newUserData)));
		this.props.closeSettings();
	}
	render() {
		var userData = this._getUserData();
		if(!this.props.visible) return null;
		return (
			<div className="modal-back" ref="back" onClick={this._close.bind(this)}>
				<form className="settings-form" onSubmit={this._saveUserData.bind(this)}>
					<span className="title">Settings</span>
					<div className="login-input">
						<input type="text" placeholder="Login" ref="login" defaultValue={userData.login} required pattern="\S(.*\S)?"/>
					</div>
					<div className="password-input">
						<input type="text" placeholder="Password" ref="password" defaultValue={userData.password} required pattern="\S+"/>
					</div>
					<input type="submit" value="Save"/>
				</form>
			</div>
		)
	}
}
export default Control;