import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './Modal.js';
import MyTable from './MyTable.js';
import Control from './Control.js';

class MyComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rows: this.getRowsData(), //Масив з рядками, напр. [{Name:"Bohdan",....}]
			visible: false, /*Видимість модального вікна*/
			btn_enabled: false, //Активність кнопок Delete та Edit
			autoSave: false, // Активність автозбереження
			modal: { // Інформація щодо модального вікна, яке використовується для реалізації функцій Add та Edit
				title: "Add item",
				submitListener: this.addRow.bind(this), // Обробник 
				btn_text: "Save",
				data: {},
				idx: null,
				callback: Function.prototype /* Допоміжна функція, використовується нижче |_|*/
			}
		}
	}
	/*Метод для отримання найбільшого значення ID, використовується при додаванні рядків*/
	_getLastID() {
		if(this.state.rows.length == 0) return 1;
		
		return Math.max.apply(Math,this.state.rows.map(function(o){return o.ID;})) + 1;
	}
	/*Отримання даних рядків з localStorage*/
	getRowsData() {
			var rows = JSON.parse(localStorage.getItem('rows')) || [];
		return rows;
	}
	/*Збереження даних рядків в localStorage*/
	saveRowsData() {
		localStorage.setItem('rows', JSON.stringify(this.state.rows));
	}
	/*Активація та деактивації функції автозбереження*/
	setAutoSave(autoSave) {
		this.setState({
			autoSave: autoSave
		})
	}
	/* Функція для відображення модального вікна для додавання рядка*/
	showAddPopup() {
		var modal = this.state.modal;
		Object.assign(modal, {
								title: "Add item",
								submitListener: this.addRow.bind(this),
								btn_text: "Save",
								data: {}
							});
		
		this.setState({
			visible: true,
			modal: modal
		})
	}
	/* Функція для відображення модального вікна для редагування рядка*/
	showEditPopup(idx /*масив ідентифікаторів рядка які потрібно відредагувати*/) {
		if(!idx.length) return;
		var current = idx.shift(), // отримуємо перший ідентифікатор
			modal = this.state.modal;
		
		Object.assign(modal, { // Редагуємо дані модального вікна
				title: "Edit item", // Змінюємо заголовок і тд.
				submitListener: this.editRow.bind(this), // Змінюємо обробник відсилання форми
				btn_text: "Save",
				data: this.state.rows[current], //передаємо дані рядка, який необхідно відредагувати
				idx: current,
				callback: function(idx) { // встановлюємо функцію колбек, використовується тоді, коли користувач
					this.showEditPopup(idx); // обрав декілька рядків, при цьому форма редагування відображається для
				}.bind(this, idx) 				// кожного з обраних рядків з відповідними даними
			});
		this.setState({
			visible: true,
			modal: modal
		})
	}
	//Метод для створення рядка
	addRow(rowData /*Дані нового рядка*/) {
		var now = new Date();
		rowData.ID = this._getLastID(); 				// Додаємо
		rowData.Date = now.toLocaleDateString('en-US'); // Допоміжні
		rowData.Time = now.toLocaleTimeString('RU');	// Дані

		var that = this,
			newState = this.state.rows;
		
		newState.push(rowData);
		this.setState({
			rows: newState,
			visible: false
		}, function() {
			if(that.state.autoSave) { // Якщо активоване автозберігання - одразу зберігаємо рядки в сховище
				that.saveRowsData();
			}
		});
	}
	//Метод для редагування рядка
	editRow(rowData, idx) {
		var now = new Date();
		rowData.Date = now.toLocaleDateString("en-US"); // Оновлюємо
		rowData.Time = now.toLocaleTimeString("RU"); 	// Допоміжні Дані
		var newRows = this.state.rows;
		newRows[idx] = Object.assign(this.state.rows[idx], rowData);
		var that = this;
		this.setState({
			rows: newRows,
			visible: false
		}, function() {
			if(that.state.autoSave) that.saveRowsData();
			that.state.modal.callback(); // Використовується при множинному редагуванні (обрані декілька рядків)
		})
	}
	// Знищення модального вікна
	closeModal() {
		var that = this;
		this.setState({
			visible: false
		}, function() {
			that.state.modal.callback();
		})
	}
	// Активація та деактивація кнопок Delete та Edit
	enableEditAndDelete(isEnable) {
		this.setState({
			btn_enabled: isEnable
		})
	}
	// Видалення рядків
	deleteRow(rowsToDelete /*масив ідентифікаторів рядків для видалення*/) {
		var newRows = this.state.rows.filter((item, idx) => rowsToDelete.indexOf(idx) === -1); // масив рядків, який не включає "видалені"
		
		var that = this;
		this.setState({
			rows: newRows,
			btn_enabled: false
		}, function() {
			if(that.state.autoSave) {
				that.saveRowsData();
			}
		});
	}
	sortByProperty(property/*властивість, за якою потрібно сортувати*/) {
		function compare(a,b) {
		  if (a[property] < b[property])
		    return -1;
		  if (a[property] > b[property])
		    return 1;
		  return 0;
		}
		var that = this;
		this.setState({
			rows: this.state.rows.sort(compare) // Відсортовані рядки
		})
	}
	render() {
		return (
			<div>
				<Control saveRowsData={this.saveRowsData.bind(this)} 
					setAutoSave={this.setAutoSave.bind(this)}
					autoSave={this.state.autoSave}
					unAuthorize={this.props.unAuthorize}/>

				<MyTable
				key={this.state.key}
				DATA={this.state.rows} 
				enableEditAndDelete={this.enableEditAndDelete.bind(this)}
				showAddPopup={this.showAddPopup.bind(this)}
				showEditPopup={this.showEditPopup.bind(this)}
				deleteRow={this.deleteRow.bind(this)}
				sortByProperty={this.sortByProperty.bind(this)}
				btn_enabled={this.state.btn_enabled}/>
				
				<Modal visible={this.state.visible} titles={["Name", "Salary", "Department"]}
					title={this.state.modal.title}
					submitListener={this.state.modal.submitListener}
					data={this.state.modal.data}
					idx={this.state.modal.idx}
					closeModal={this.closeModal.bind(this)}
					btn_text={"Save"}/>
			</div>
		);
	}
}
ReactDOM.render(<MyComponent/>, document.querySelector('#app'));
export default MyComponent;