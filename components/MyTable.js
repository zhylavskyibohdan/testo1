import React from 'react';
import ReactDOM from 'react-dom';
import reactDragula from 'react-dragula';
class MyTable extends React.Component {
	constructor(props) {
		super(props);

		var rowState = [];
		for(let i=0; i<this.props.DATA.length; i++) { 
			rowState[i]=false;
		}
		this.state = {
			checkAll: false, // Активний "головний" чекбокc
			rowState: rowState, // масив, який містить інформацію про обрані рядки
			i: 0 // службова змінна
		}
	}
	componentDidMount() { /* Ініціалізація DragnDrop*/
	    var container = document.querySelector('.table-body'),
	    	drake = reactDragula([container], {
	    		mirrorContainer: document.querySelector(".hidden")
	    	});
	  }
	componentWillReceiveProps(nextProps) {
		if(nextProps.DATA.length > this.state.rowState.length) { /*Якщо був доданий новий рядок*/
			var newRowState = this.state.rowState;
			newRowState.push(false);

			this.setState({ /*Оновлюємо інформацію про чекбокси + додаємо новий "стан"*/
				rowState: newRowState
			})
		}
	}
	/*Метод, який викликається коли користувач активував або деактивував "головний" чекбокс*/
	handleSelectAll(e) {
		var rowState =[],
			checkState = !this.state.checkAll;
		    
		/*Встановлюємо значення кожного чекбокса, ідентичне "головному"(активне чи неактивне)*/
	    for(var i = 0; i < this.state.rowState.length; i++) {
	      rowState[i] = checkState;
	    }
		this.state.checkAll = checkState;

		var that = this;
		   this.setState({
		     rowState: rowState,
		     checkAll: this.state.checkAll
		   }, function() {
		   	 that.props.enableEditAndDelete(that.state.rowState.some((el) => el == true)) // Якщо хоча б один активний,
		   	 																				// то активовуємо кнопки Edit та Delete
		   });
	}
	/*Лістенер для кожного з чекбоксів*/
	handleCheckOne(idx /*ідентифікатор чек*/, value /*значення*/) {
		this.state.rowState[idx] = value;
	    if (this.state.checkAll) { 
	      this.state.checkAll = !this.state.checkAll; /*Деактивуємо головний чек*/
	    }
	    var that = this;
	    this.setState({
	      rowState: this.state.rowState,
	      checkAll: this.state.checkAll
	    }, function() {
	    	that.props.enableEditAndDelete(that.state.rowState.some((el) => el == true))
	    });
	}
	// видалення рядка
	_deleteRow() {
		var rowsToDelete = []; //ідентифікатори рядків для видалення
		this.state.rowState.forEach((row, idx) => {if(row) rowsToDelete.push(idx);});
		this.props.deleteRow(rowsToDelete);


		var newRowState = this.state.rowState.filter((item, idx) => rowsToDelete.indexOf(idx) === -1);
		this.setState({
			rowState: newRowState,
			checkAll: this.state.checkAll ? !this.state.checkAll : this.state.checlAll
		})
	}
	// редагування рядка
	_editRow() {
		var rowsToEdit = [];
		this.state.rowState.forEach((row, idx) => {if(row) rowsToEdit.push(idx);});
		this.props.showEditPopup(rowsToEdit);
	}
	render() {
		var key = new Date().getMinutes();
		return (
			<div>
				<div className="table-container">
					<div className="table">
						<HeaderRow selectAll={this.handleSelectAll.bind(this)} 
							checked={this.state.checkAll}
							sortByProperty={this.props.sortByProperty}/>
							<div className="table-body">
								{	
									this.props.DATA.map((row_data,idx) => {return <Row data={row_data} key={this.state.i++} idx={idx} checked={this.state.rowState[idx]} callback={this.handleCheckOne.bind(this)}/>})
								}
							</div>
					</div>
				</div>
				<div className="btn-container">
					<button className="btn btn-add" onClick={this.props.showAddPopup}>Add</button>
					<button className="btn btn-edit" disabled={!this.props.btn_enabled} onClick={this._editRow.bind(this)}>Edit</button>
					<button className="btn btn-delete" disabled={!this.props.btn_enabled}
						onClick={this._deleteRow.bind(this)}>Delete</button>
				</div>
			</div>
		);
	}
}

/*Заголовочний рядок*/
class HeaderRow extends React.Component {
	constructor(props) {
		super(props); 
	}
	/*Сортування за властивістю, викликається при кліку по заголовку стовпця*/
	sortByProperty(property) {
		var that = this;
		/*Додаємо ефект активності (CSS клас)*/
		Object.keys(this.refs).forEach(function(key) {
			that.refs[key].classList.remove("sorted");
		})
		this.refs[property].classList.add('sorted')
		this.props.sortByProperty(property);
		
	}
	render() {
		return (
			<div className="row head-title">
				<div className="col col-1">
					<input type="checkbox" id={"check" + this.props.idx} className="checkbox" onChange={this.props.selectAll} checked={this.props.checked}/>
					<label htmlFor={"check" + this.props.idx} className="check-label"></label>
				</div>
				<div className="col col-1" ref="ID">
					<span onClick={this.sortByProperty.bind(this, 'ID')}>ID</span>
				</div>
				<div className="col col-2" ref="Date">
					<span onClick={this.sortByProperty.bind(this, 'Date')}>Date</span>
				</div>
				<div className="col col-2" ref="Time">
					<span onClick={this.sortByProperty.bind(this, 'Time')}>Time</span>
				</div>
				<div className="col col-2" ref="Name">
					<span onClick={this.sortByProperty.bind(this, 'Name')}>Name</span>
				</div>
				<div className="col col-2" ref="Salary">
					<span onClick={this.sortByProperty.bind(this, 'Salary')}>Salary</span>
				</div>
				<div className="col col-2" ref="Department">
					<span onClick={this.sortByProperty.bind(this, 'Department')}>Department</span>
				</div>
			</div>
		);	
	}
}

/*Рядок таблиці*/
class Row extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			checked: this.props.checked
		}
	}
	/*Натиснутий чек*/
	handleCheck(e) {
		this.props.callback(this.props.idx, !this.props.checked);
	}
	render() {
		return (
			<div className="row">
				<div className="col col-1">
					<input type="checkbox" id={"check" + this.props.idx} className="checkbox" checked={this.props.checked} onChange={this.handleCheck.bind(this)}/>
					<label htmlFor={"check" + this.props.idx} className="check-label"></label>
				</div>
				<div className="col col-1">
					{this.props.data.ID}
				</div>
				<div className="col col-2">
					{this.props.data.Date}
				</div>
				<div className="col col-2">
					{this.props.data.Time}
				</div>
				<div className="col col-2">
					{this.props.data.Name}
				</div>
				<div className="col col-2">
					{this.props.data.Salary}
				</div>
				<div className="col col-2">
					{this.props.data.Department}
				</div>
			</div>
		);
	}
}
export default MyTable;