# Worker Manager

###### В процесі виконання тестового завдання були реалізовані такі функції:
  - Авторизація + Можливість довготривалої авторизації (не потрібно постійно авторизовуватись при вході на сайт)
  - Передбачена функція зміни логіну та паролю
  - Передбачена можливість автозберігання, при цьому користувач сам обирає спосіб зберігання.
  - Розроблено весь функціонал додавання, редагування та видалення рядків, який передбачений в технічному завданні
  - Розроблено функціонал по сортуванню рядків. Для сортування рядків за певним стовпцем, потрібно клікнути по заголовку цього стовпця.
  - Розроблена функція Drag&Drop для рядків (взято готовий варіант), проте дана функція не працює так, як я цього хотів, зокрема не зберігаються інформація про перестановку стовпців
  - Розроблено інтерфейс який коректно відображається на мобільних пристроях (принаймі в мене:)). При малому розширенні екрана, таблиця набуває сталої ширини та з'являється скрол. Для прокрутки таблиці, потрібно користуватися заголовочним рядком. 

Ознайомитись з сайтом можна тут: [Worker Manager](https://zhylavskyibohdan-testo1-staging.surge.sh/)